# Dropbox Mass Restore
# Copyright (C) 2013 Maarten Kossen (mpkossen), Quateria
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Include the Dropbox SDK libraries
from dropbox import client, rest, session
import ConfigParser

config = ConfigParser.ConfigParser()
config.read('dropbox-mass-restore.cfg')

# Get your app key and secret from the Dropbox developer website
APP_KEY = config.get('AppSettings', 'app_key')
APP_SECRET = config.get('AppSettings', 'app_secret')

# ACCESS_TYPE should be 'dropbox' or 'app_folder' as configured for your app
ACCESS_TYPE = 'dropbox'

sess = session.DropboxSession(APP_KEY, APP_SECRET, ACCESS_TYPE)

request_token = sess.obtain_request_token()

url = sess.build_authorize_url(request_token)

# Make the user sign in and authorize this token
print "url:", url
print "Please visit this website and press the 'Allow' button, then hit 'Enter' here."
raw_input()

# This will fail if the user didn't visit the above URL and hit 'Allow'
access_token = sess.obtain_access_token(request_token)
print access_token.key
print access_token.secret

client = client.DropboxClient(sess)
print "linked account:", client.account_info()