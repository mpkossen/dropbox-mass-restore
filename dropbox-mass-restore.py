# Dropbox Mass Restore
# Copyright (C) 2013 Maarten Kossen (mpkossen), Quateria
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Include the Dropbox SDK libraries
from dropbox import client, rest, session
from datetime import datetime, timedelta, time
import ConfigParser

config = ConfigParser.ConfigParser()
config.read('dropbox-mass-restore.cfg')

# Get your app key and secret from the Dropbox developer website
APP_KEY = config.get('AppSettings', 'app_key')
APP_SECRET = config.get('AppSettings', 'app_secret')

# ACCESS_TYPE should be 'dropbox' or 'app_folder' as configured for your app
ACCESS_TYPE = 'dropbox'

sess = session.DropboxSession(APP_KEY, APP_SECRET, ACCESS_TYPE)

sess.set_token(config.get('OAuthSettings', 'oauth_key'), config.get('OAuthSettings', 'oauth_secret'))

client = client.DropboxClient(sess)
# print "linked account:", client.account_info()

def restoreFiles(path):
	folder_metadata = client.metadata(path,include_deleted=True)
	#print "metadata:", folder_metadata

	since = datetime(2013, 01, 18, 18, 00, 00)
	before = datetime(2013, 01, 20, 18, 00, 00)
	for data in folder_metadata.keys():
		if data == 'contents':
			for fileInfo in folder_metadata[data]:
				modified = datetime.strptime(fileInfo['modified'], "%a, %d %b %Y %H:%M:%S +0000")
				if 'is_deleted' in fileInfo.keys() and modified > since and modified < before and fileInfo['is_deleted'] == True and fileInfo['is_dir'] == False:
						print 'Current file:'
						print fileInfo['path']
						revisions = client.revisions(fileInfo['path'])
						for revision in revisions:
							revDate = datetime.strptime(revision['modified'], "%a, %d %b %Y %H:%M:%S +0000")
							if 'is_deleted' in revision.keys():
								print 'Skipping revision that deletes a file'
							else:
								# Restore
								print 'Restoring file...'
								client.restore(revision['path'], revision['rev'])
								print 'File restored'
								break;
				else:
					if 'is_dir' in fileInfo.keys() and fileInfo['is_dir'] == True and (('is_deleted' in fileInfo.keys() and fileInfo['is_deleted'] == False) or 'is_deleted' not in fileInfo.keys()):
						# Call function on dir
						print 'Calling restoreFiles for folder:'
						print fileInfo['path']
						restoreFiles(fileInfo['path'])

path='/'
restoreFiles(path)

print "All done"

#TODO Check for removed directories